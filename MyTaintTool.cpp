
// mix of Example 2 - http://shell-storm.org/blog/Taint-analysis-with-Pin/
// and source/tools/ManualExamples/proccount.cpp of Pin

#include "pin.H"
#include <asm/unistd.h>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <map>
#include <list>
#include <algorithm>
#include <string.h>

ofstream debug_out;

/* ========================================================================= */

typedef struct RegTaint {
  REG reg;
  unsigned int index;

  RegTaint(REG reg) : reg(reg), index(-1) {}
  RegTaint(REG reg, unsigned int index) : reg(reg), index(index) {}

  bool operator==(const RegTaint& rt) const { return reg == rt.reg; }
} RegTaint;

/* ========================================================================= */

std::list<RegTaint> regs_tainted;
std::map<UINT64, unsigned int> mem_tainted;

/* ========================================================================= */

void add_mem_taint(UINT64, unsigned int);
void remove_mem_taint(UINT64);
bool is_mem_tainted(UINT64);
unsigned int get_mem_taint_index(UINT64);

/* ========================================================================= */

void add_reg_taint(REG, unsigned int);
void remove_reg_taint(REG);
bool is_reg_tainted(REG reg);
unsigned int get_reg_taint_index(REG reg);

/* ========================================================================= */

void add_mem_taint(UINT64 addr, unsigned int index) {
  mem_tainted[addr] = index;
}

void remove_mem_taint(UINT64 addr) {
  if (!is_mem_tainted(addr))
    return;

  mem_tainted.erase(addr);
}

bool is_mem_tainted(UINT64 addr) {
  return mem_tainted.count(addr) > 0;
}

unsigned int get_mem_taint_index(UINT64 addr) {
  return mem_tainted[addr];
}

/* ========================================================================= */

void add_reg_taint(REG reg, unsigned int index) {
  if (!REG_valid(reg))
    return;

  remove_reg_taint(reg);

  switch(reg){

  case REG_RAX:  regs_tainted.push_front(RegTaint(REG_RAX, index));
  case REG_EAX:  regs_tainted.push_front(RegTaint(REG_EAX, index));
  case REG_AX:   regs_tainted.push_front(RegTaint(REG_AX, index));
  case REG_AH:   regs_tainted.push_front(RegTaint(REG_AH, index));
  case REG_AL:   regs_tainted.push_front(RegTaint(REG_AL, index));
       break;

  case REG_RBX:  regs_tainted.push_front(RegTaint(REG_RBX, index));
  case REG_EBX:  regs_tainted.push_front(RegTaint(REG_EBX, index));
  case REG_BX:   regs_tainted.push_front(RegTaint(REG_BX, index));
  case REG_BH:   regs_tainted.push_front(RegTaint(REG_BH, index));
  case REG_BL:   regs_tainted.push_front(RegTaint(REG_BL, index));
       break;

  case REG_RCX:  regs_tainted.push_front(RegTaint(REG_RCX, index));
  case REG_ECX:  regs_tainted.push_front(RegTaint(REG_ECX, index));
  case REG_CX:   regs_tainted.push_front(RegTaint(REG_CX, index));
  case REG_CH:   regs_tainted.push_front(RegTaint(REG_CH, index));
  case REG_CL:   regs_tainted.push_front(RegTaint(REG_CL, index));
       break;

  case REG_RDX:  regs_tainted.push_front(RegTaint(REG_RDX, index));
  case REG_EDX:  regs_tainted.push_front(RegTaint(REG_EDX, index));
  case REG_DX:   regs_tainted.push_front(RegTaint(REG_DX, index));
  case REG_DH:   regs_tainted.push_front(RegTaint(REG_DH, index));
  case REG_DL:   regs_tainted.push_front(RegTaint(REG_DL, index));
       break;

  case REG_RDI:  regs_tainted.push_front(RegTaint(REG_RDI, index));
  case REG_EDI:  regs_tainted.push_front(RegTaint(REG_EDI, index));
  case REG_DI:   regs_tainted.push_front(RegTaint(REG_DI, index));
  case REG_DIL:  regs_tainted.push_front(RegTaint(REG_DIL, index));
       break;

  case REG_RSI:  regs_tainted.push_front(RegTaint(REG_RSI, index));
  case REG_ESI:  regs_tainted.push_front(RegTaint(REG_ESI, index));
  case REG_SI:   regs_tainted.push_front(RegTaint(REG_SI, index));
  case REG_SIL:  regs_tainted.push_front(RegTaint(REG_SIL, index));
       break;

  default:
    return;
  }
}

void remove_reg_taint(REG reg) {
  if (!REG_valid(reg))
    return;

  switch(reg){

  case REG_RAX:  regs_tainted.remove(RegTaint(REG_RAX));
  case REG_EAX:  regs_tainted.remove(RegTaint(REG_EAX));
  case REG_AX:   regs_tainted.remove(RegTaint(REG_AX));
  case REG_AH:   regs_tainted.remove(RegTaint(REG_AH));
  case REG_AL:   regs_tainted.remove(RegTaint(REG_AL));
       break;

  case REG_RBX:  regs_tainted.remove(RegTaint(REG_RBX));
  case REG_EBX:  regs_tainted.remove(RegTaint(REG_EBX));
  case REG_BX:   regs_tainted.remove(RegTaint(REG_BX));
  case REG_BH:   regs_tainted.remove(RegTaint(REG_BH));
  case REG_BL:   regs_tainted.remove(RegTaint(REG_BL));
       break;

  case REG_RCX:  regs_tainted.remove(RegTaint(REG_RCX));
  case REG_ECX:  regs_tainted.remove(RegTaint(REG_ECX));
  case REG_CX:   regs_tainted.remove(RegTaint(REG_CX));
  case REG_CH:   regs_tainted.remove(RegTaint(REG_CH));
  case REG_CL:   regs_tainted.remove(RegTaint(REG_CL));
       break;

  case REG_RDX:  regs_tainted.remove(RegTaint(REG_RDX));
  case REG_EDX:  regs_tainted.remove(RegTaint(REG_EDX));
  case REG_DX:   regs_tainted.remove(RegTaint(REG_DX));
  case REG_DH:   regs_tainted.remove(RegTaint(REG_DH));
  case REG_DL:   regs_tainted.remove(RegTaint(REG_DL));
       break;

  case REG_RDI:  regs_tainted.remove(RegTaint(REG_RDI));
  case REG_EDI:  regs_tainted.remove(RegTaint(REG_EDI));
  case REG_DI:   regs_tainted.remove(RegTaint(REG_DI));
  case REG_DIL:  regs_tainted.remove(RegTaint(REG_DIL));
       break;

  case REG_RSI:  regs_tainted.remove(RegTaint(REG_RSI));
  case REG_ESI:  regs_tainted.remove(RegTaint(REG_ESI));
  case REG_SI:   regs_tainted.remove(RegTaint(REG_SI));
  case REG_SIL:  regs_tainted.remove(RegTaint(REG_SIL));
       break;

  default:
    return;
  }
}

bool is_reg_tainted(REG reg) {
  std::list<RegTaint>::iterator it = std::find(
    regs_tainted.begin(), regs_tainted.end(), RegTaint(reg));

  return it != regs_tainted.end();
}

unsigned int get_reg_taint_index(REG reg) {
  std::list<RegTaint>::iterator it = std::find(
    regs_tainted.begin(), regs_tainted.end(), RegTaint(reg));

  if (it != regs_tainted.end())
    return it->index;

  return -1;
}

/* ========================================================================= */

VOID ReadMem(INS ins, UINT64 addr, CONTEXT *ctx) {
  if (INS_OperandCount(ins) != 2)
    return;

  REG reg = INS_OperandReg(ins, 0);

  if (is_mem_tainted(addr)) {
    unsigned int index = get_mem_taint_index(addr);
    char* cp = (char*) (addr);
    std::cerr << "[READ] "
              << "char=" << *cp << " "
              << "index=" << index
              << std::endl;

    add_reg_taint(reg, index);
  } else if (is_reg_tainted(reg)) {
    std::cerr << "[READ] "
              << "reg=" << REG_StringShort(reg) << " "
              << "index=" << get_reg_taint_index(reg)
              << std::endl;

    remove_reg_taint(reg);
  }


}

VOID WriteMem(INS ins, UINT64 addr) {
  if (INS_OperandCount(ins) != 2)
    return;

  REG reg = INS_OperandReg(ins, 1);

  remove_mem_taint(addr);

  if (is_reg_tainted(reg))
    add_mem_taint(addr, get_reg_taint_index(reg));
}

VOID SpreadRegTaint(INS ins) {
  if (INS_OperandCount(ins) != 2)
    return;

  REG reg_read = INS_RegR(ins, 0);
  REG reg_write = INS_RegW(ins, 0);

  if (is_reg_tainted(reg_read)) {
    add_reg_taint(reg_write, get_reg_taint_index(reg_read));
  }
  else {
    remove_reg_taint(reg_write);
  }
}

VOID Instruction(INS ins, VOID *v) {
  if (INS_OperandCount(ins) > 1 && INS_MemoryOperandIsRead(ins, 0) &&
      INS_OperandIsReg(ins, 0)) {
    INS_InsertCall(ins, IPOINT_BEFORE, (AFUNPTR)ReadMem, IARG_PTR, ins,
                   IARG_MEMORYOP_EA, 0, IARG_CONTEXT, IARG_END);
  } else if (INS_OperandCount(ins) > 1 && INS_MemoryOperandIsWritten(ins, 0)) {
    INS_InsertCall(ins, IPOINT_BEFORE, (AFUNPTR)WriteMem, IARG_PTR, ins,
                   IARG_MEMORYOP_EA, 0, IARG_END);
  } else if (INS_OperandCount(ins) > 1 && INS_OperandIsReg(ins, 0)) {
    INS_InsertCall(ins, IPOINT_BEFORE, (AFUNPTR)SpreadRegTaint, IARG_PTR, ins,
                   IARG_END);
  }
}

static unsigned int tryksOpen;

#define TRICKS()                                                               \
  {                                                                            \
    if (tryksOpen++ == 0)                                                      \
      return;                                                                  \
  }

VOID Syscall_entry(THREADID thread_id, CONTEXT *ctx, SYSCALL_STANDARD std,
                   void *v) {

  if (PIN_GetSyscallNumber(ctx, std) == __NR_read) {
    TRICKS(); /* tricks to ignore the first open */

    UINT64 start = static_cast<UINT64>((PIN_GetSyscallArgument(ctx, std, 1)));
    UINT64 size = static_cast<UINT64>((PIN_GetSyscallArgument(ctx, std, 2)));

    for (unsigned int i = 0; i < size; i++)
      add_mem_taint(start + i, i);

    std::cerr << "[MEM TAINT] bytes tainted from " << std::hex << "0x"
               << start << " to 0x" << start + size << " (via read)"
               << std::endl;
  }
}

/* ========================================================================= */

typedef struct RtnInfo {
  string _name;
  string _image;
  ADDRINT _address;
  RTN _rtn;
} RTN_INFO;

VOID rtn_ent_cb(RTN_INFO *ri) {
  if (ri->_image == "calc_parse") {
    std::cerr << "[ENTER] " << ri->_name << std::endl;
  }
}

VOID rtn_ret_cb(RTN_INFO *ri) {
  if (ri->_image == "calc_parse") {
    std::cerr << "[EXIT] " << ri->_name << std::endl;
  }
}

const char *StripPath(const char *path) {
  const char *file = strrchr(path, '/');
  if (file)
    return file + 1;
  else
    return path;
}

// Pin calls this function every time a new rtn is executed
VOID Routine(RTN rtn, VOID *v) {

  // Allocate a counter for this routine
  RTN_INFO *ri = new RTN_INFO;

  ri->_name = RTN_Name(rtn);
  ri->_image = StripPath(IMG_Name(SEC_Img(RTN_Sec(rtn))).c_str());
  ri->_address = RTN_Address(rtn);

  RTN_Open(rtn);

  RTN_InsertCall(rtn, IPOINT_BEFORE, (AFUNPTR)rtn_ent_cb, IARG_PTR, ri,
                 IARG_END);

  RTN_InsertCall(rtn, IPOINT_AFTER, (AFUNPTR)rtn_ret_cb, IARG_PTR, ri,
                 IARG_END);

  RTN_Close(rtn);
}

/* ===================================================================== */

INT32 Usage() {
  std::cerr << "my taint tool" << std::endl;
  return -1;
}

int main(int argc, char *argv[])

{
  // Initialize symbol table code, needed for rtn instrumentation
  PIN_InitSymbols();

  //debug_out.open("debug.out");

  if (PIN_Init(argc, argv)) {
    return Usage();
  }

  // for instruction based taint
  PIN_SetSyntaxIntel();
  PIN_AddSyscallEntryFunction(Syscall_entry, 0);
  INS_AddInstrumentFunction(Instruction, 0);

  // for routine entry and exit
  RTN_AddInstrumentFunction(Routine, 0);

  PIN_StartProgram();

  return 0;
}
