echo "Building calc_parse..."
make -C ./calc_parse

echo "Building MyTaintTool..."
make

echo "Running pin.sh with MyTaintTool and calc_parse..."
echo "1+2" | ./../../../pin -t obj-intel64/MyTaintTool.so -- calc_parse/calc_parse
